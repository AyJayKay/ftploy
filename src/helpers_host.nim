import os, strutils, strformat, options, sugar, nre, sequtils, algorithm
import asyncdispatch, patched_asyncftpclient
import constants, data, helpers

proc formatHostHashes*(raw: string, context: Context): seq[(string, string, string)] =
    for line in raw.split("\n"):
        let parts = line.split(hashSep)
        if (parts.len >= 2) and parts[1].isNotIgnored(context):
            let hash =
                if parts.len == 3: parts[2].strip()
                else: ""
            let path = parts[1].strip().replace("/", $os.DirSep)
            result.add((parts[0].strip(), path, hash))

proc getHostFolderEntriesBasedOn*(raw, currentPath: string, context: Context): seq[(string, string)] =
    let rawList = raw.split("\n").filterIt(it.strip().len > 0)
    for line in rawList:
        let matches = line.match(re"^(.)[rwx-]*.* (.*)")
        if matches.isSome():
            let groups = (i: int) => matches.get().captures[i]
            let name = groups(1)
            let hostPath = currentPath / name

            if name in @[".", ".."] or (currentPath.normalizedPath == context.remoteData.normalizedPath and name.strip() == context.metaFileName):
                continue

            let entryPath = hostPath.relativePath(context.remoteData)
            if entryPath.isNotIgnored(context):
                let typePrefix =
                    if groups(0) == "d": "d"
                    else: "f"
                
                result.add((typePrefix, entryPath))
    if result.len == 0 and rawList.len > 2 :
        echo "FTP server 'LIST' result:"
        echo raw
        raiseError "It seems like the server uses an unknown list return..."

proc findOutdatedHashes*(local, host: seq[(string, string, string)]): seq[(string, string, string)] =
    for currentEntry in host:
        for controlEntry in local:
            if currentEntry[1] == controlEntry[1]:
                if currentEntry[2] != controlEntry[2]:
                    result.add(currentEntry)
                continue

proc echoServerWelcome*(msg: string) =
    if msg.len > 0:
        echo "Server says: \""
        echo msg.indent(4)
        echo "\"\n"

proc getHostEntries*(ftp: AsyncFtpClient, context: Context): Future[seq[(string, string)]] {.async.} =
    var hostEntries = newSeq[(string, string)]()
    var dirsToCheck = @["."]
    while dirsToCheck.len > 0:
        let currentDir = context.remoteData / dirsToCheck.pop()
        if currentDir.relativePath(context.remoteData).isNotIgnored(context) or currentDir.isAbsolute():
            await ftp.cd(currentDir)
            let rawEntries = await ftp.list()
            let entries = rawEntries.getHostFolderEntriesBasedOn(currentDir, context)
            dirsToCheck &= (entries.filterIt(it[0] == "d").mapIt(it[1]))
            hostEntries &= entries
    return hostEntries

proc retrieveHostsHashes*(ftp: AsyncFtpClient, context: Context): Future[seq[(string, string, string)]] {.async.} =
    let hashFilePath = context.remoteData / context.metaFileName
    let isMetaFileExisting = await ftp.existsFile(hashFilePath)
    if not isMetaFileExisting:
        let errorMsg = fmt"'{hashFilePath}' can not be found on host!" & "\n"
        if context.isForceMode:
            echo errorMsg
        else:
            raiseError errorMsg
    else:
        let rawHostHashes = await ftp.retrText(hashFilePath)
        result = rawHostHashes.formatHostHashes(context)
        if context.isCheckMode:
            echoHashes(result)
            echo ""

proc renewHashFile*(ftp: AsyncFtpClient, localHashes: seq[(string, string, string)], context: Context) {.async.} =
    if not context.isCheckMode:
        let hashFilePath = context.remoteData / context.metaFileName
        let isMetaFileExisting = await ftp.existsFile(hashFilePath)
        if isMetaFileExisting:
            await ftp.removeFile(context.remoteData / context.metaFileName)

        cleanupTempDir(context)
        createDir(context.tempDirPath)

        var file = open(context.tempDirPath / context.metaFileName, fmWrite)
        for i in localHashes:
            let path = i[1].normalizePathSep()
            file.writeLine fmt"{i[0]}{hashSep}{path}{hashSep}{i[2]}"
        file.writeLine ""
        file.close()
        await ftp.store(context.tempDirPath / context.metaFileName, context.remoteData / context.metaFileName)
        cleanupTempDir(context)

proc deleteByIndexes*(ftp: AsyncFtpClient, hostEntryIndexesToDelete: seq[string], context: Context) {.async.} =
    # DESC -> fist files than folders
    for entry in hostEntryIndexesToDelete.sorted(order = Descending):
        let entryType = $entry[0]
        let path = entry[2..<entry.len]

        echo entry.indent(4)
        if context.isCheckMode: continue

        if entryType == "f":
            await ftp.removeFile(context.remoteData / path)
        if entryType == "d":
            # todo check for ignored content and handle not deleting
            await ftp.removeDir(context.remoteData / path)

proc uploadByIndexes*(ftp: AsyncFtpClient, localEntryIndexesToUpload: seq[string], context: Context) {.async.} =
    # ASC -> first folders than files
    #     -> shorter paths first -> folder parents first
    for entry in localEntryIndexesToUpload.sorted(order = Ascending):
        let entryType = $entry[0]
        let path = entry[2..<entry.len]

        echo entry.indent(4)
        if context.isCheckMode: continue

        if entryType == "d":
            await ftp.createDir(context.remoteData / path)
        if entryType == "f":
            await ftp.store(context.localData / path, context.remoteData / path)