import os, std/sha1, helpers
import data

proc createLocalHashData*(context: Context): seq[(string, string, string)] =
    echo "create local hashes..."
    for path in walkDirRec(context.localData, {pcFile, pcDir}):
        if path.relativePath(context.localData).isNotIgnored(context):
            let (typePrefix, hash) =
                if path.existsDir:
                    ("d", "")
                else:
                    ("f", $secureHashFile(path))
            let relPath = path.relativePath(context.localData)
            result.add((typePrefix, relPath, hash))
    
    if context.isCheckMode:
        echoHashes(result)
        echo ""