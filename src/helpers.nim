import os, strformat, strutils, sequtils, algorithm, nre
import constants, data

proc tempDirPath*(c: Context): string = c.localData / (appName & "-tmp")

proc raiseError*(message: string) =
    echo "Error: " & message
    quit(1)

proc isNotIgnored*(relPath: string, context: Context): bool =
    if relPath.isAbsolute():
        raiseError "IsNotIgnored is used with absolute path!"
    
    context.ignoreExpressions.allIt(relPath.match(it).isNone())

proc rawToRegex*(multilineRaw: string): seq[Regex] =
    multilineRaw.split("\n").filterIt(it.len > 0).mapIt(re(it.strip()))

proc getEntryIndexes*(sequence: seq[any]): seq[string] = sequence.mapIt(it[0] & hashSep & it[1]).sorted()

proc findFilePathUpwards*(path, name: string): string =
    let filePath = path / name
    if fileExists(filePath):
        return filePath
    else:
        if path.isRootDir:
            raiseError fmt"{name} could not be found!"
        return findFilePathUpwards(path.parentDir, name)

proc echoHashes*(hashes: seq[(string, string, string)]) =
    if hashes.len > 0:
        let maxPathLength = hashes.mapIt(it[1].len).max() + 1
        for (_, path, hash) in hashes.sortedByIt(it[1]):
            echo indent(path, 4) & ' '.repeat(maxPathLength - path.len) & hash

proc cleanupTempDir*(context: Context) =
    if dirExists(context.tempDirPath): removeDir(context.tempDirPath)