import strutils, options, sequtils, algorithm, terminal
import asyncdispatch, patched_asyncftpclient
import configs, data, helpers, helpers_host, helpers_local

proc main(context: Context, localHashes: seq[(string, string, string)]) {.async.} =
    let ftp = newAsyncFtpClient(
        context.remoteHost,
        port = Port(context.remotePort),
        user = context.user,
        pass = context.password
    )
    echo "connecting..."
    let connection = ftp.connect()
    await connection or sleepAsync(5000)

    if connection.finished and not connection.failed:
        echo "connected!"
        echoServerWelcome(connection.read)

        echo "retrieving hashes from host..."
        let hostHashes = await ftp.retrieveHostsHashes(context)

        echo "scanning host file system..."
        let hostEntries = await ftp.getHostEntries(context)
        
        echo "sanity check for host hashes..."
        let hostEntriesIndexes = hostEntries.getEntryIndexes()
        let hostHashesIndexes = hostHashes.getEntryIndexes()
        if hostEntriesIndexes != hostHashesIndexes:
            echo "Hashed but not existing on host:"
            for i in hostHashesIndexes.filterIt(it notin hostEntriesIndexes):
                echo i
            echo ""

            echo "Existing on host but not hashed or ignored:"
            for i in hostEntriesIndexes.filterIt(it notin hostHashesIndexes):
                echo i.indent(4)
            echo ""

            let errorMsg = "Host's hash file is not in sync with it's file system!"
            if context.isForceMode:
                echo errorMsg
            else:
                raiseError errorMsg
        
        echo "comparing local to host..."
        if context.isClearingHostDemanded:
            styledEcho styleBright, fgRed, "Prune-mode is active. (Everything should be cleared and redeployed.)"
        elif context.isForceMode:
            styledEcho styleBright, fgRed, "Force-mode is active. (Ignoring mismatches or missing hashes.)"

        let localEntriesIndexes = localHashes.getEntryIndexes()
        let hostEntryIndexesToDelete =
            if context.isClearingHostDemanded:
                hostEntriesIndexes
            else:
                (
                    (hostEntriesIndexes.filterIt(it notin localEntriesIndexes)) &
                    (findOutdatedHashes(localHashes, hostHashes).getEntryIndexes())
                ).deduplicate()

        let hostRemainingEntryIndexes = hostEntriesIndexes.filterIt(it notin hostEntryIndexesToDelete)
        let localEntryIndexesToUpload = localEntriesIndexes.filterIt(it notin hostRemainingEntryIndexes)

        echo ""

        if context.isCheckMode:
            echo "What would be deployed:"
        else:
            echo "Start deployment:"

        if hostEntryIndexesToDelete.len > 0:
            echo "deleting outdated..."
            await ftp.deleteByIndexes(hostEntryIndexesToDelete, context)

        if localEntryIndexesToUpload.len > 0:
            echo "upload..."
            await ftp.uploadByIndexes(localEntryIndexesToUpload, context)
        
        if localHashes.sorted() != hostHashes.sorted():
            echo "update hash file..."
            await ftp.renewHashFile(localHashes, context)

        echo "Done."

    elif connection.failed:
        raise connection.readError

    else:
        raiseError "Connection timed out!"

# main
let context = readConfigsAndArguments()

cleanupTempDir(context)

let localHashes = createLocalHashData(context)

waitFor(main(context, localHashes))
