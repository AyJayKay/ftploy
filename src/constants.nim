import nre

const appName* = "ftploy"
const metaFileDefaultName* = "hashes.meta"
const hashSep* = ":"
let constantIgnores* = @[re"(.*[/\\])?\.ftpquota", re"(.*[/\\])?\.DS_Store"]