import nre

type
    Context* = object
        remoteHost*: string
        remotePort*: int
        user*, password*: string
        
        localData*, remoteData*: string
        metaFileName*: string
        ignoreExpressions*: seq[Regex]

        isCheckMode*: bool
        isForceMode*: bool
        isClearingHostDemanded*: bool
