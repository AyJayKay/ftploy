import parsecfg, os, strformat, options, strutils, sequtils, nre
import helpers, constants, data

proc get(conf: Config, key: string, optionalDefault: Option[string] = none(string)): string =
    let value = conf.getSectionValue("", key)
    if value == "":
        if optionalDefault.isSome():
            return optionalDefault.get()
        else:
            raiseError fmt"Key '{key}' is not configured!"
    return value.strip()

proc absolutePathOrRelativeTo(path, base: string): string =
    let output =
        if path.isAbsolute():
            path
        else:
            base / path
    if not existsDir(output):
        raiseError fmt"Local folder does not exist: {output}"
    return output

proc readConfigsAndArguments*(): Context =
    echo "gathering configs and arguments..."
    let configPath = getCurrentDir().findFilePathUpwards(appName & ".conf")
    
    let config = loadConfig(configPath)
    let credentials = loadConfig(getCurrentDir().findFilePathUpwards(appName & ".cred"))

    let args = commandLineParams().mapIt(it.split(" ")).foldl(a & b, newSeq[string]()).mapIt(it.strip())
    let isCheckMode = args.anyIt("check" in it)
    let isPruneMode = "--forceprune" in args
    let isForceMode = "--force" in args or isPruneMode

    echo "validating..."
    result = Context(
        remoteHost: config.get("remote"),
        remotePort: config.get("port").parseInt,
        user: credentials.get("usr"),
        password: credentials.get("cred", optionalDefault = some("")),

        localData: config.get("localdata").absolutePathOrRelativeTo(configPath.parentDir),
        remoteData: config.get("remotedata").normalizePathEnd,
        metaFileName: config.get("meta", some(metaFileDefaultName)),
        ignoreExpressions: config.get("ignore", some("")).rawToRegex() & constantIgnores,

        isCheckMode: isCheckMode,
        isForceMode: isForceMode,
        isClearingHostDemanded: isPruneMode
    )

    if not result.remoteData.isAbsolute:
        raiseError "Remote data path needs to the absolute!"
    echo "configs look good!"