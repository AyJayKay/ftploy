from os import `/`, relativePath

let pathToRoot = thisDir().relativePath(system.getCurrentDir())
let mainSource = pathToRoot / "src" / "main.nim"
let compileOutput = pathToRoot / "out"

task dev, "Builds and runs non release build. Use 'args \"...\"' to append params.":
    setCommand "compile", mainSource
    switch "out", compileOutput / "ftploy_dev"
    switch "run"

proc zipOutput(target, source: string) =
    let path = compileOutput / (target & ".zip")
    if fileExists(path): exec "rm '" & path & "'"
    exec "cd '" & compileOutput & "' && zip -r " & target & ".zip " & source & " -x '*.DS_Store' -x '__MACOSX'"

task build_mac, "Builds a release build for macOS":
    exec "nim c -d:release --os:macOSX --stackTrace=on --listFullPaths=off -o:'" & (compileOutput / "mac" / "ftploy") & "' " & mainSource
    zipOutput("mac", "mac")

task build_win, "Builds a release build for Windows (https://nim-lang.org/docs/nimc.html#cross-compilation-for-windows)":
    proc buildCmd(cpu: string): string =
        "nim c -d:mingw -d:release --stackTrace=on --listFullPaths=off --cpu:" & cpu & " -o:'" & (compileOutput / cpu / "ftploy") & "' " & mainSource

    proc pack(cpu, dll, target: string) =
        if fileExists(compileOutput / cpu / dll):
            exec buildCmd cpu
            zipOutput(target, cpu)
        else:
            echo "###############################################"
            echo dll & " not existing in " & compileOutput / cpu
            echo "copy it from 'dlls' folder to run packing"
    
    pack "i386", "pcre32.dll", "win32"
    pack "amd64", "pcre64.dll", "win64"


task build, "Run all builds":
    exec "nim build_mac"
    exec "nim build_win"