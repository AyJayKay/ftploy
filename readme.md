# Welcome to Ftploy!

Ftploy (pronounced [ɛf tiː plɔɪ] or "F-Teploy") is a simple cli tool to deploy files via ftp connection.
It will one-way-synchronize adding, updating and deleting files as well as adding and deleting folders.
Ftploy pushes a meta file with work file hashes to the remote folder and uses that to only push new or updated files.


Feedback, Bugs: https://ayjaykay.com/#about

# Get it

The release notes contain simple download links:

[GitLab releases](https://gitlab.com/AyJayKay/ftploy/-/releases)

Or you [compile it yourself](#developer).

# Set up

A config needs to be called `ftploy.conf`.
A credentials file needs to be called `ftploy.cred`. I recommend to NOT save your credentials in your project's version control system. (E.g. put it in your gitignore.)

Both files need to be located in the execution's working directory or any of its parents.
(Upward search -> First hits are used.)

`ftploy.conf` field examples:

    # connection details
    remote="my-awesome-host.com"
    port=21

    # local folder of content
    # if relative path: relative to the config file
    localdata="/user/my-awesome-project/build/"

    # remote folder path
    # absolute (ftp user path on host)
    remotedata="/public/serve/"

    # optional
    # custom name for text meta file
    # default: 'hashes.meta'
    meta="my-hashes.txt"

    # optional
    # regular expressions for ignoring path's (local and remote)
    # use 3 double quotes for multiple entries:
    ignore="""
    (.*/)?all-files-with-this-name\.txt
    (.*/)?all-folders-with-this-name(/.*)?
    under-root/file\.txt
    under-root/folder(/.*)?
    """

`ftploy.cred` field examples:

    usr="jon.dow"
    cred="5UP3R 5ICR3T P455W0RT"

**Hint:** In the example the _content_ of `/user/my-awesome-project/build/`, plus the `my-hashes.txt` will end up as the new _content_ of the `serve` folder of the host.

**Trivia:** Ftploy knows some constant names to ignore like macOS' `DS_Store`...

## Usage:

If you just execute the app it will validate configs, perform a normal check of the remote's hashes and its file system and will start deployment.

Consider following options:

    --check

Just get a **preview of what would be deleted and uploaded**. This will also print out acknowledged **hashes in human readable form**.

Use `check` to e.g. verify your path settings or ignore expressions. Or it can also be used together with the following options to see what would be different.

---

    --force

This ignores remote hashes and performs delete and upload only based on file existence. It will overwrite the remote hashes.

Use `force` to e.g. **introduce Ftploy** to a remote that already exists and you just want to push the hash file.

---

    --forceprune

This is like `force` but also **clears the remote data folder** and pushes all files freshly.

Use this, if you break your synchronization with the `force`... I told you, Luke...

# Trivia

Ftploy is developed for a host on unix based file system. It assumes the server to respond with corresponding file and path information.


Do not use a folder called `ftploy-tmp` inside your local data folder. It will be overwritten and removed.


And as of Ftploy v1.1.1 or lower:

Do not use spaces in your folder and file names.

Do not use `:` in your folder and file names.

# Developer

Ftploy is written in [NIM](https://nim-lang.org/) 1.2.6.

Nim tasks are set up.

Use `nim help` to view them.

Use `nim dev args "your-runtime-arguments"` to build and run a dev build into `out/`. The config rules apply like for the normal build.